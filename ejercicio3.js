//Esta función sólo comprueba si el documento es válido
function compruebadocumento(numero){
    if (numero < 0 || numero >99999999){
        return false;
    } else {
        return true;
    }
}
//Calcula la letra que le correspondería según el valor de DNI introducido
function calculaLetra(valor){
    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E']
    var resultado;
    var operador;
    operador = valor % 23;
    resultado = letras [operador];
    return (resultado);
}
var documento = prompt('Introduzca su DNI:','');
//Si el documento no es válido, lanza el mensaje y se detiene la ejecución del script
if (compruebadocumento(documento) == false){
    console.log('El documento introducido no es válido');
//si el documento es válido (no sale la comprobación "false") el script continua
} else {
    var letra = prompt('Introduzca la letra de su DNI:','');
    var letraReal = calculaLetra(documento);
    if (letra == letraReal){
        console.log('DNI Válido. Benvenido Avenger.');
    } else {
        console.log('DNI Incorrecto.');
    }
}